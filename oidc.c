/*
 * Functionalities related to OpenId Connect based authentication
 */

#include "oidc.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>
#include <cjson/cJSON.h>
#include <oidc-agent/api.h>
#include "lib/logc/log.h"

#ifdef _MSV_VER
#define strdup(p) _strdup(p)
#endif

accesstoken_response *get_accesstoken (char *oidc_configuration){
    log_debug("accesstoken_response");
    accesstoken_response *pq = malloc(sizeof(accesstoken_response));

    pq->accesstoken = getAccessToken(oidc_configuration, 60, NULL, "putty-ssh-client", NULL);
    return pq;
}

struct memory {
    char *response;
    size_t size;
};

static size_t cb (void *data, size_t size, size_t nmemb, void *userp){
    size_t realsize    = size * nmemb;
    struct memory *mem = (struct memory *)userp;

    char *ptr = realloc(mem->response, mem->size + realsize + 1);

    if (ptr == NULL)
        return 0;

    mem->response = ptr;
    memcpy(&(mem->response[mem->size]), data, realsize);
    mem->size               += realsize;
    mem->response[mem->size] = 0;

    return realsize;
}

deployed_user *deploy_user (char *hostname, char *accesstoken, char *oidc_motley_cue_host,
                            int oidc_motley_cue_port){
    static struct memory chunk;

    log_debug("deploy_user");
    deployed_user *pq = malloc(sizeof(deployed_user));

    // Sanity check for oidc configuration
    if (accesstoken == NULL) {
        log_error("No accesstoken was provided");
        pq->deployed = false;
        return pq;
    }

    CURL *curl;
    CURLcode res;
    cJSON *ssh_user;

    curl = curl_easy_init();
    if (curl) {
        char endpoint[1028];
        if (oidc_motley_cue_host == NULL){
            sprintf(endpoint, "%s/user/deploy", hostname);
        } else {
            sprintf(endpoint, "%s/user/deploy", oidc_motley_cue_host);
        }
        int port;
        if (oidc_motley_cue_port != 0)
            port = oidc_motley_cue_port;
        else
            port = 8080;
        log_debug("Sending deploy request to: %s:%d", endpoint, port);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_easy_setopt(curl, CURLOPT_URL, endpoint);
        curl_easy_setopt(curl, CURLOPT_PORT, port);
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2);

        struct curl_slist *headers = NULL;
        char header_label[128]     = "Authorization: Bearer ";
        char *authorization;
        size_t header_len = strlen(header_label) + strlen(accesstoken) + 1;
        authorization = malloc(header_len);
        snprintf(authorization, header_len, "%s%s", header_label, accesstoken);

        headers = curl_slist_append(headers, authorization);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

        res = curl_easy_perform(curl);

        long response_code;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        if ((res != CURLE_OK) || (response_code != 200)) {
            log_error("HTTP request to deploy endpoint was not successful: code %d", response_code);
            pq->deployed = false;
            curl_easy_cleanup(curl);
            return pq;
        }
        log_debug("Received deploy user response: %d", response_code);
        curl_easy_cleanup(curl);
        free(authorization);
    }
    cJSON *json  = cJSON_Parse(chunk.response);
    cJSON *state = cJSON_GetObjectItem(json, "state");

    if (strcmp(state->valuestring, "deployed") != 0) {
        log_error("Returned user was not deployed");
        pq->deployed = false;
        return pq;
    }
    cJSON *credentials = cJSON_GetObjectItem(json, "credentials");

    ssh_user     = cJSON_GetObjectItem(credentials, "ssh_user");
    pq->username = strdup(ssh_user->valuestring);
    pq->deployed = true;
    log_debug("User was successfully deployed: %s", ssh_user->valuestring);
    return pq;
}

#ifdef _WIN32
oidc_configurations *get_loaded_oidc_configurations (){
    char *configs_str = getLoadedAccountsList();

    log_debug("get_loaded_oidc_configurations");
    if (configs_str != NULL) {
        cJSON *json              = cJSON_Parse(configs_str);
        oidc_configurations *ocs = malloc(sizeof(oidc_configurations));
        ocs->n_confs        = cJSON_GetArraySize(json);
        ocs->configurations = malloc(sizeof(oidc_configuration *) * ocs->n_confs);
        for (int i = 0; i < ocs->n_confs; i++) {
            oidc_configuration *oc = malloc(sizeof(oidc_configuration));
            oc->configuration_name = cJSON_GetArrayItem(json, i)->valuestring;
            oc->id                 = i;
            ocs->configurations[i] = oc;
        }
        secFree(configs_str);
        return ocs;
    }else
        return NULL;
}
#endif

void free_oidc_configurations (oidc_configurations *ocs){
    size_t i;

    log_debug("free_oidc_configurations");
    for (i = 0; i < ocs->n_confs; i++) {
        oidc_configuration *oc = ocs->configurations[i];
        free(oc);
    }
    free(ocs->configurations);
    free(ocs);
}

supported_ops *get_supported_ops_for_server (char *hostname, uint32_t port){
    log_debug("get_supported_ops_for_server");
    static struct memory chunk;
    supported_ops *sup_ops = malloc(sizeof(supported_ops));
    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();
    if (curl) {
        char endpoint[1024];
        sprintf(endpoint, "%s/info", hostname);
        if (port == 0)
            port = 8080;
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_easy_setopt(curl, CURLOPT_URL, endpoint);
        curl_easy_setopt(curl, CURLOPT_PORT, port);
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

        res = curl_easy_perform(curl);

        long response_code;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        if ((res != CURLE_OK) || (response_code != 200)) {
            log_error("HTTP request to deploy endpoint was not successful");
            return NULL;
        }
        curl_easy_cleanup(curl);
    }else {
        log_error("CURL could not be initialized");
        return NULL;
    }
    log_debug("Received info response");
    cJSON *json     = cJSON_Parse(chunk.response);
    cJSON *ops_json = cJSON_GetObjectItem(json, "supported_OPs");
    cJSON *op       = NULL;

    sup_ops->n_ops = cJSON_GetArraySize(ops_json);
    sup_ops->ops   = malloc(sup_ops->n_ops * sizeof(char *));
    size_t i = 0;

    cJSON_ArrayForEach(op, ops_json){
        sup_ops->ops[i++] = strdup(cJSON_GetStringValue(op));
    }
    cJSON_Delete(json);
    log_debug("Successfully received supported OPs from endpoint");
    return sup_ops;
}
// vim: textwidth=100
