include(GNUInstallDirs)

find_library(oidc-agent_LIBRARY
  NAMES oidc-agent)

find_path(oidc-agent_INCLUDE_DIR
  NAMES oidc-agent/api.h)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(oidc-agent DEFAULT_MSG
                                  oidc-agent_LIBRARY
                                  oidc-agent_INCLUDE_DIR)

mark_as_advanced(oidc-agent_LIBRARY oidc-agent_INCLUDE_DIR)

if(oidc-agent_FOUND AND NOT TARGET oidc-agent::oidc-agent)
  add_library(oidc-agent::oidc-agent SHARED IMPORTED)
  set_target_properties(oidc-agent::oidc-agent
    PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${oidc-agent_INCLUDE_DIR}"
      IMPORTED_LOCATION ${oidc-agent_LIBRARY})
endif()
