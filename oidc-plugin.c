#ifdef _WIN32
#include <windows.h>
#include <ShlObj.h>
#else
#endif
#include <stdio.h>
#include <unistd.h>
#ifdef _WIN32
#include <share.h>
#else
#endif
#include "stdint.h"
#include "plugin-ki.h"
#include "parser.h"
#include "oidc.h"
#include "marshal.h"
#include "cache.h"
#include <oidc-agent/api.h>
#include "lib/logc/log.h"
#include "lib/argparse/argparse.h"

#define BUFSIZE 1024

FILE *Logfile;
typedef struct {
    int loglevel;
    char *mcue_host;
    uint32_t mcue_port;
    char *agent_config;
    char *issuer;
} args_struct;
args_struct Args;

int read_pipe(char *buf);
int write_pipe(char *buf, int len);
void handle_message(const plugin_ki_message *msg);
void setup_handles ();
void setup_logfile();
void parse_args(int argc, const char **argv);
void send_init_failure(char *message);
uint32_t handle_version_check(uint32_t max_remote_version);
char *prompt_user_for_oidc_config(char *host_string);

typedef struct {
    uint32_t version;
    ptrlen hostname;
    uint32_t port;
    ptrlen username;
    char *issuer;

    // Contains which state the protocol is currently in
    char current_state;

    supported_ops *ops;
    char *accesstoken;
} protocol_state;
protocol_state State;

int main (int argc, const char **argv){
    parse_args(argc, argv);
    log_set_quiet(true);

#ifdef _WIN32
    setup_handles();
    setup_logfile();
#endif

    log_info("\n\nStarted OIDC KI Plugin");
    // Beginning null state
    State.current_state = 0;
    while (!(State.current_state == PLUGIN_AUTH_SUCCESS ||
             State.current_state == PLUGIN_AUTH_FAILURE)) {
        /**log_debug("main loop: current_state: %d", State.current_state);*/
        char buf[BUFSIZE];
        int len = read_pipe(buf);

        if (len < 0)
            continue;

        plugin_ki_message *msg = malloc(sizeof(plugin_ki_message));
        char type              = parse_ki_message(buf, msg);
        if (type == PLUGIN_INVALID_MESSAGE) {
            log_error("Received invalid message");
            int ret = write_pipe(buf, len);

            if (ret < 0)
                continue;
        }else{
            handle_message(msg);
            free_ki_message(msg);
        }
    }
    if (State.ops) {
        for (size_t i = 0; i < State.ops->n_ops; i++) {
            log_debug("    freeing State.ops %d", i);
            free(State.ops->ops[i]);
            log_debug("    freed   State.ops %d", i);
        }
    }
    free(State.ops);
    secFree(State.accesstoken);
    log_debug("Terminating successfully");
    // trying the less successful operations in silence:
    fclose(Logfile);
    return 0;
}

#ifdef _WIN32
HANDLE hStdin, hStdout;
void setup_handles (){
    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    hStdin  = GetStdHandle(STD_INPUT_HANDLE);
    if (
        (hStdout == INVALID_HANDLE_VALUE) ||
        (hStdin == INVALID_HANDLE_VALUE))
        ExitProcess(1);
}

void setup_logfile (){
    wchar_t *local_app_data_path;

    if (SUCCEEDED(SHGetKnownFolderPath(&FOLDERID_LocalAppData, 0, NULL, &local_app_data_path))) {
        wchar_t folder_path[1024];
        swprintf_s(folder_path, 1024, L"%s\\oidc-ki-plugin", local_app_data_path);
        CoTaskMemFree(local_app_data_path);
        DWORD dwAttrib = GetFileAttributesW(folder_path);
        if (!(dwAttrib != INVALID_FILE_ATTRIBUTES &&
              (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)))
            CreateDirectoryW(folder_path, NULL);
        wchar_t log_file_path[1024];
        swprintf_s(log_file_path, 1024, L"%s\\debug.log", folder_path);
        /**if (_wfopen_s(&Logfile, log_file_path, L"a+") == 0)*/
        Logfile = _wfsopen(log_file_path, L"a+", _SH_DENYNO);
        if (Logfile != NULL)
            log_add_fp(Logfile, Args.loglevel);
    }
}

#endif


static void dump (const char *text, unsigned char *ptr, size_t size){
    size_t i;
    size_t c;
    unsigned int width = 0x10;
    char buf[256];

    sprintf(buf, "%s - %ld bytes (0x%lx)",
            text, (long)size, (long)size);
    log_debug(buf);
    sprintf(buf, "%s - ", text);

    for (i = 0; i < size; i += width) {
        sprintf(buf, "%s%4.4lx: ", buf, (long)i);

        /* show hex to the left */
        for (c = 0; c < width; c++) {
            // insert spacer
            if ((c % (width / 2)) == 0 && c > 0)
                sprintf(buf, "%s: ", buf);
            else if ((c % (width / 4)) == 0 && c > 0)
                sprintf(buf, "%s ", buf);

            if (i + c < size)
                sprintf(buf, "%s%02x ", buf, ptr[i + c]);
            else
                sprintf(buf, "%s   ", buf);
        }

        /* show data on the right */
        sprintf(buf, "%s -  ", buf);
        for (c = 0; (c < width) && (i + c < size); c++) {
            // spacer
            if ((c % (width / 4)) == 0 && c > 0)
                sprintf(buf, "%s ", buf);
            char x = (ptr[i + c] >= 0x20 && ptr[i + c] < 0x80) ? ptr[i + c] : '.';
            sprintf(buf, "%s%c", buf, x);
        }

        log_debug(buf); /* write output */
        sprintf(buf, "%s - ", text);

        // skip middle, if size too large
        if (size > 200) {
            if (i >= 2 * width && i < 3 * width) {
                i = size - 3 * width;
                log_debug(
                    "%s - [.................................... skipped ..................................]",
                    text);
            }
        }
    }
}

int read_pipe (char *buf){
#ifdef _WIN32
    DWORD dwRead;
    static int nothing_read_counter = 0;
    int bSuccess;
    bSuccess = ReadFile(hStdin, buf, BUFSIZE, &dwRead, NULL);
    if (!bSuccess || dwRead == 0) {
        usleep(200000);
        if (nothing_read_counter >= 3) {
            log_error("Pipe closed unexpectedly, terminating plugin");
            exit(0);
        }
        nothing_read_counter++;
        return -1;
    }
    buf[dwRead] = '\0';
    dump("read_pipe ", buf, dwRead);
    return dwRead;
#else
    return read(STDIN_FILENO, buf, BUFSIZE);
#endif
}

int write_pipe (char *buf, int len){
#ifdef _WIN32
    DWORD dwWritten;

    log_debug("writing %d (0x%x) bytes", len, len);
    dump("write_pipe", buf, len);
    WriteFile(hStdout, buf, len, &dwWritten, NULL);
    return dwWritten;
#else
    return write(STDOUT_FILENO, buf, len);
#endif
}

void send_init_failure (char *message){
    log_debug("send_init_failure: %s", message);
    strbuf *buf = make_strbuf();

    buf->len = 4;
    put_byte(buf, PLUGIN_INIT_FAILURE);
    put_string(buf, message, strlen(message));

    PUT_32BIT_MSB_FIRST(buf->data, buf->len - 4);
    char *str = strbuf_to_str(buf);

    write_pipe(str, buf->len + 0);
    log_debug("buf->len: %d", buf->len);
    free_strbuf(buf);
    State.current_state = PLUGIN_INIT_FAILURE;
    log_debug("Sent init failure message");
    free(State.ops);
    log_debug("Exit Code: 22\n");
    fclose(Logfile);
    exit(22);
}

void send_init_response (){
    log_debug("send_init_reponse");

    strbuf *buf = make_strbuf();
    buf->len = 4;
    put_byte(buf, PLUGIN_INIT_RESPONSE);
    put_uint32(buf, State.version);
    put_string(buf, State.username.ptr, State.username.len);

    PUT_32BIT_MSB_FIRST(buf->data, buf->len - 4);
    char *str = strbuf_to_str(buf);

    log_debug("Sending init response (current state: %d)", State.current_state);
    log_debug("    version: >%d<", State.version);
    log_debug("    usrname: >%s<", State.username.ptr);

    write_pipe(str, buf->len);
    free_strbuf(buf);
    State.current_state = PLUGIN_INIT_RESPONSE;
}

uint32_t handle_version_check (uint32_t max_remote_version){
    if (max_remote_version < PLUGIN_PROTOCOL_VERSION) {
        log_debug("Supports protocol version %d, we need %d. Failure",
                  max_remote_version, PLUGIN_PROTOCOL_VERSION);
        State.current_state = PLUGIN_INIT_FAILURE;
        char buf[1024];
        snprintf(buf, 1024, "Plugin requires protocol version %d.\n"
                 "Please update your putty client to at least version %s",
                 PLUGIN_PROTOCOL_VERSION, PLUGIN_PUTTY_VERSION_KNOWN_TO_WORK);
        send_init_failure(buf);
        return 0; // actuall, send_init_failure wll terminate this session
    } else
        return PLUGIN_PROTOCOL_VERSION;
}
void handle_plugin_init (const plugin_ki_init *msg){
    log_debug("handle_plugin_init");
    State.current_state = PLUGIN_INIT;
    uint32_t max_remote_version = msg->version;

    State.hostname    = dup_ptrlen(&msg->hostname);
    State.port        = msg->port;
    State.username    = dup_ptrlen(&msg->username);
    State.accesstoken = NULL;

    log_debug("Received plugin Init");
    log_debug("hostname %s:%d", ptrlen_to_str(&State.hostname), State.port);
    log_debug("suggested username: %s", ptrlen_to_str(&State.username));
    log_debug("plugin_init: max version from server: %d", max_remote_version);

    State.version = handle_version_check(max_remote_version);   // terminates, if version fails

    if (State.accesstoken == NULL){
    // 1: check if short name was given on commandline
        if (Args.agent_config){
            log_debug("Using agent_config from commandline: %s", Args.agent_config);
            State.accesstoken = getAccessToken(Args.agent_config, 60, NULL, \
                    "PuTTY credential plugin", NULL);
        }
    }
    if (State.accesstoken == NULL){
    // 2: check if url was given on commandline
        if (Args.issuer){
            log_debug("Using issuer_url from commandline: %s", Args.issuer);
            State.issuer      = malloc(strlen(Args.issuer)); // do not update the cache
            State.issuer      = strdup(Args.issuer);
            State.accesstoken = getAccessTokenForIssuer(Args.issuer, 60, NULL, \
                    "PuTTY credential plugin", NULL);
        }
    }
    if (State.accesstoken == NULL){
    // 3: check if there's a cache entry
        char *cached_issuer = cache_get_issuer_for_host(ptrlen_to_str(&State.hostname), State.port);
        log_debug("Cached issuer found, using: %s", cached_issuer);
        State.accesstoken = getAccessTokenForIssuer(cached_issuer, 60, NULL, "PuTTY credential plugin", NULL);
        State.issuer      = malloc(strlen("cached")); // do not update the cache
        sprintf(State.issuer, "cached");
        log_debug("Got accesstoken");
    }
    if (State.accesstoken == NULL){
    // 4: prompt user
        char *host_string = ptrlen_to_str(&State.hostname);
        log_debug("No cached issuer found, getting supported OPs from server");
        // Check if using custom mcue host
        if (Args.mcue_host != NULL)
            host_string = Args.mcue_host;
        State.ops = get_supported_ops_for_server(host_string, Args.mcue_port);
        if (State.ops == NULL || State.ops->n_ops <= 0) {
            log_error("No available OPs sent from server");
            send_init_failure("No available OPs sent from server");
        } else {// we got a list of issuers and now prompt the user
            char *issuer = prompt_user_for_oidc_config(host_string);
            if (!issuer) {
                log_error("No issuer specified by the user: FATAL");
                send_init_failure("No issuer specified by the user: FATAL");
            }
            log_debug("User selected issuer: %s", issuer);
            State.issuer      = strdup(issuer);
            State.accesstoken = getAccessTokenForIssuer(issuer, 60, NULL, "PuTTY credential plugin", NULL);
            free(issuer);
        }
    }
    if (State.accesstoken != NULL) {
        if (State.issuer != NULL){
            log_debug("Got accesstoken from issuer %s (len: %d)", State.issuer, strlen(State.accesstoken));
        } else {
            log_debug("Got accesstoken from config %s (len: %d)", Args.agent_config, strlen(State.accesstoken));
        }
        log_debug("deploying user via Args.mcue_host: >%s:%d<", Args.mcue_host, Args.mcue_port);
        deployed_user *user = deploy_user(ptrlen_to_str(&State.hostname), State.accesstoken, 
                Args.mcue_host, Args.mcue_port);
        if (user->deployed) {
            State.username = make_ptrlen(strdup(user->username), strlen(user->username));
        } else {
            char message[1024];
            snprintf(message, 1024, "Error while deploying the user");
            log_error(message);
            send_init_failure(message);
        }

        log_debug("calling send_init_response");
        send_init_response();
    } else {
        log_error("Could not get accesstoken");
        char message[1024];
        snprintf(message, 1024, "oidc-agent error message: %s", oidcagent_serror());
        send_init_failure(message);
    }
}

char *prompt_user_for_oidc_config (char *host_string) {
    char *command = malloc(256);

    snprintf(command, 256, "oidc-prompt select \"Identity Chooser\" \"<h2>Choose your Identity</h2>"\
            "<p/>"\
            "Please choose the AAI which provides your Identity "\
            "for ssh at "\
            "<b>%s</b>.\" "\
            "\"Pick one from the list\" ", host_string);

    size_t command_len = strlen(command);

    for (int i = 0; i < State.ops->n_ops; i++) {
        command_len += strlen(State.ops->ops[i]) + 3;  // 3 for these: " "
        command      = realloc(command, command_len+1);

        strcat(command, " \"");
        strncat(command, State.ops->ops[i], strlen(State.ops->ops[i]));
        strcat(command, "\"");
        log_debug("added issuer>%s<", State.ops->ops[i]);
    }
    log_debug("oidc-prompt cmdline: >%s<", command);

    FILE *fd;
#ifdef _WIN32
    fd = _popen(command, "rt");
    HWND window_handle;
    window_handle = GetConsoleWindow();
    ShowWindow(window_handle, SW_HIDE);
#else
    fd = popen(command, "rt");
#endif
    if (!fd) return NULL;

    char buffer[256];
    size_t chread;
    /* String to store entire command contents in */
    size_t comlen = 1;
    char  *comout = malloc(comlen);

    strncpy(comout, "", 1);
    memset(buffer, 0, 256);

    /* Use fread so binary data is dealt with correctly */
    while ((chread = fread(buffer, 1, sizeof(buffer), fd)) != 0) {
        comlen += strlen(buffer);
        comout  = realloc(comout, comlen);
        strncat(comout, buffer, comlen);
        memset(buffer, 0, 256);
    }
    // strip trailing newline, if it exists
    int length = strlen(comout);

    if (comout[length - 1] == '\n')
        comout[length - 1] = '\0';
    comout[strlen(comout)] = 0;
    free(command);
    pclose(fd);
    return comout;
}

void handle_plugin_protocol (const plugin_ki_protocol *prot){
    log_debug("handle_plugin_protocol");
    strbuf *buf = make_strbuf();

    buf->len = 4;

    char type = PLUGIN_INVALID_MESSAGE;

    if (strcmp(ptrlen_to_str(&prot->id), PLUGIN_KI_AUTH_METHOD_ID) == 0) {
        type = PLUGIN_PROTOCOL_ACCEPT;
        put_byte(buf, type);
        log_debug("Sending:: PLUGIN_PROTOCOL_ACCEPT (%d)", type);
    } else {
        type = PLUGIN_PROTOCOL_REJECT;
        put_byte(buf, type);
        log_error("Not supported protocol: %s", ptrlen_to_str(&prot->id));
        log_error("Sending: PLUGIN_PROTOCOL_REJECT (%d)", type);
        put_string(buf, "", strlen(""));
    }
    PUT_32BIT_MSB_FIRST(buf->data, buf->len - 4);
    char *str = strbuf_to_str(buf);

    write_pipe(str, buf->len);
    free_strbuf(buf);
    State.current_state = type;
}

void handle_plugin_ki_server_request (const plugin_ki_server_request *prot){
    log_debug("handle_plugin_ki_server_request");
    strbuf *buf = make_strbuf();

    buf->len = 4;
    if (prot->num_prompts == 1 &&
        strcmp(ptrlen_to_str(&prot->prompts[0].prompt), "Access Token:") == 0) {
        put_byte(buf, PLUGIN_KI_SERVER_RESPONSE);
        put_uint32(buf, 1);
        put_stringi(buf, State.accesstoken);
        State.current_state = PLUGIN_KI_SERVER_RESPONSE;
    } else {
        if (prot->num_prompts == 0) { // we did not get any prompt, which
                                      // could be fine. We simply answer as empty.
            put_byte(buf, PLUGIN_KI_SERVER_RESPONSE);
            put_uint32(buf, 0);
            State.current_state = PLUGIN_KI_SERVER_RESPONSE;
        } else {
            log_error("Got an unexpected prompt that can not be processed");
            put_byte(buf, PLUGIN_AUTH_FAILURE);
            State.current_state = PLUGIN_AUTH_FAILURE;
        }
    }
    PUT_32BIT_MSB_FIRST(buf->data, buf->len - 4);
    char *str = strbuf_to_str(buf);

    write_pipe(str, buf->len);
    free_strbuf(buf);
}
void handle_plugin_auth_success (){
    log_debug("handle_plugin_auth_success");

    /**State.username = dup_ptrlen(&msg->username);*/
    State.current_state = PLUGIN_AUTH_SUCCESS;

    // issuer should always be known, but we better double check:
    if (!State.issuer)
        log_error("Issuer was not set; Not able to store it in chache");
    else if (strcmp(State.issuer, "cached") != 0) // only update the cache, if needed
        cache_set_issuer_for_host(ptrlen_to_str(&State.hostname), State.port, State.issuer);
}

void handle_message (const plugin_ki_message *msg){
    log_debug("handle_message type >%d<", *msg->type);
    switch (*msg->type) {
    case PLUGIN_INIT:
        handle_plugin_init(msg->init);
        break;
    case PLUGIN_PROTOCOL:
        handle_plugin_protocol(msg->protocol);
        break;
    case PLUGIN_KI_SERVER_REQUEST:
        handle_plugin_ki_server_request(msg->server_request);
        break;
    case PLUGIN_KI_USER_RESPONSE:
        // Should not be received in the current implementation
        break;
    case PLUGIN_AUTH_SUCCESS:
        handle_plugin_auth_success();
        break;
    case PLUGIN_AUTH_FAILURE:
        State.current_state = PLUGIN_AUTH_FAILURE;
        break;
    }
}

static const char *const usages[] = {
    "basic [options] [[--] args]",
    "basic [options]",
    NULL,
};

void parse_args (int argc, const char **argv){
    log_debug("parse_args");
    // Pre-initialize args
    Args.loglevel     = LOG_INFO;
    Args.mcue_host    = NULL;
    Args.mcue_port    = 0;
    Args.agent_config = NULL;
    Args.issuer       = NULL;

    // Parse args
    int debug                        = 0;
    char *mcue_host                  = NULL;
    uint32_t mcue_port               = 0;
    char *agent_config               = NULL;
    char *issuer                     = NULL;
    struct argparse_option options[] = {
        OPT_HELP(),
        OPT_GROUP("Basic options"),
        OPT_BOOLEAN('d', "debug"    , &debug       , "Log debug output"                    , NULL, 0, 0) ,
        OPT_STRING('m' , "mcue-host", &mcue_host   , "Custom motley cue host"              , NULL, 0, 0) ,
        OPT_INTEGER('p', "mcue-port", &mcue_port   , "Custom motley cue port"              , NULL, 0, 0) ,
        OPT_STRING('o' , "oidc"     , &agent_config, "oidc-agent configuration name to use", NULL, 0, 0) ,
        OPT_STRING('i' , "issuer"   , &issuer      , "oidc-provider (OP) URL to use"       , NULL, 0, 0) ,
        OPT_END(),
    };

    struct argparse argparse;

    argparse_init(&argparse, options, usages, 0);
    argparse_describe(&argparse, "\nOIDC KI Auth Plugin", "");
    argparse_parse(&argparse, argc, argv);

    log_debug("mcue-host %s", mcue_host);
    log_debug("mcue-port %d", mcue_port);

    if (debug != 0)
        Args.loglevel = LOG_DEBUG;
    if (mcue_host != NULL)
        Args.mcue_host = mcue_host;
    if (mcue_port != 0)
        Args.mcue_port = mcue_port;
    if (agent_config != NULL)
        Args.agent_config = agent_config;
    if (issuer != NULL)
        Args.issuer = issuer;
}
// vim: textwidth=100
