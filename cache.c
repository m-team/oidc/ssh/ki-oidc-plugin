#include "cache.h"
#include "lib/logc/log.h"

#ifdef _WIN32
#include <Windows.h>
#define PLUGIN_REGISTRY_KEY "SOFTWARE\\oidc-ki-plugin"
#endif

char *read_cache(const char *key);
bool write_cache(const char *key, const char *val);

#ifdef _WIN32
char *cache_get_issuer_for_host (const char *hostname, uint32_t port){
    char port_string[16];
    char *cache_string;
    size_t cache_string_len;

    snprintf(port_string, 16, "%d", port);
    cache_string_len = strlen(hostname) + 2 + strlen(port_string);
    cache_string     = malloc(cache_string_len);
    snprintf(cache_string, cache_string_len, "%s:%s", hostname, port_string);
    log_debug("trying to get cache entry for %s", cache_string);

    return read_cache(cache_string);
}

bool cache_set_issuer_for_host (const char *hostname, uint32_t port, const char *issuer){
    char port_string[16];
    char *cache_string;
    size_t cache_string_len;

    snprintf(port_string, 16, "%d", port);
    cache_string_len = strlen(hostname) + 2 + strlen(port_string);
    cache_string     = malloc(cache_string_len);
    snprintf(cache_string, cache_string_len, "%s:%s", hostname, port_string);
    log_debug("going to set cache entry for %s: %s", cache_string, issuer);

    /**FIXME*/
    /**free(cache_string);*/
    return write_cache(cache_string, issuer);
}
#else
char *cache_get_issuer_for_host (const char *hostname, uint32_t port){
    const char *cache_string="not yet implemented";
    return read_cache(cache_string);
}

bool cache_set_issuer_for_host (const char *hostname, uint32_t port, const char *issuer){
    const char *cache_string="not yet implemented";
    return write_cache(cache_string, issuer);
}

#endif

#ifdef _WIN32
char *read_cache (const char *key){
    DWORD bufferSize = 8192;
    char *buf        = malloc(8192);
    LSTATUS err      =
        RegGetValue(HKEY_CURRENT_USER, PLUGIN_REGISTRY_KEY, key, RRF_RT_ANY, NULL, (PVOID)buf,
                    &bufferSize);

    if (err == ERROR_SUCCESS) {
        char *retBuf = malloc(strlen(buf) + 1);
        strncpy(retBuf, buf, strlen(buf) + 1);
        free(buf);
        return retBuf;
    }else{
        log_error("Could not read from registry with key: %s", key);
        return NULL;
    }
}

bool write_cache (const char *key, const char *val){
    HKEY createdKey;

    if (RegCreateKey(HKEY_CURRENT_USER, PLUGIN_REGISTRY_KEY, &createdKey) !=
        ERROR_SUCCESS) {
        log_error("Could not write to registry under key: %s", key);
        return false;
    }
    return RegSetValueEx(createdKey, key, 0, REG_SZ, (LPBYTE)val,
                         strlen(val) * sizeof(char)) == ERROR_SUCCESS;
}
#else
// Not needed for now
char *read_cache (const char *key){
    log_error("NYI");
    return NULL;
}
bool write_cache (const char *key, const char *val){
    log_error("NYI");
    return false;
}
#endif
// vim: textwidth=100
