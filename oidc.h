#ifndef _OIDC_H
#define _OIDC_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
    bool deployed;
    char *username;
} deployed_user;

typedef struct {
    char *accesstoken;
} accesstoken_response;

typedef struct {
    char *configuration_name;
    int id;
} oidc_configuration;

typedef struct {
    oidc_configuration **configurations;
    size_t n_confs;
} oidc_configurations;

typedef struct {
    char *accesstoken;
} motley_cue_request;

typedef struct {
    size_t n_ops;
    char **ops;
} supported_ops;

accesstoken_response *get_accesstoken(char *oidc_configuration);
deployed_user *deploy_user(char *hostname, char *accesstoken, char *oidc_motley_cue_host,
                           int oidc_motley_cue_port);
#ifdef _WIN32
oidc_configurations *get_loaded_oidc_configurations();
#endif
void free_oidc_configurations(oidc_configurations *ocs);
supported_ops *get_supported_ops_for_server(char *hostname, uint32_t port);

#endif /* _OIDC_H */
