#ifndef _CACHE_H
#define _CACHE_H

#include <stdbool.h>
#include "stdint.h"

char *cache_get_issuer_for_host(const char *hostname, uint32_t port);
bool cache_set_issuer_for_host(const char *hostname, uint32_t port, const char *issuer);

#endif /* _CACHE_H */
// vim: shiftwidth=2
