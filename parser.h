#ifndef _PARSER_H
#define _PARSER_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// Most of this file is taken and/or inspired from PuTTY

// Wrapper to support multi-byte encoded strings (e.g. UTF8)
typedef struct {
    const char *ptr;
    size_t len;
} ptrlen;

static inline ptrlen make_ptrlen (const char *ptr, size_t len){
    ptrlen pl;

    pl.ptr = ptr;
    pl.len = len;
    return pl;
}

static inline ptrlen dup_ptrlen (const ptrlen *ptr){
    ptrlen dup;

    dup.ptr = malloc(ptr->len);
    memcpy((void *)dup.ptr, (void *)ptr->ptr, ptr->len);
    dup.len = ptr->len;
    return dup;
}

static inline char *ptrlen_to_str (const ptrlen *ptr){
    char *str = malloc(ptr->len + 1);

    strncpy(str, ptr->ptr, ptr->len);
    str[ptr->len] = '\0';
    return str;
}

static inline uint32_t GET_32BIT_MSB_FIRST (const void *vp){
    const uint8_t *p = (const uint8_t *)vp;

    return ((uint32_t)p[3]) | ((uint32_t)p[2] << 8) |
           ((uint32_t)p[1] << 16) | ((uint32_t)p[0] << 24);
}

typedef struct {
    const char *data;
    size_t pos;
    size_t len;
} Parser;

#define GET_AVAIL (src->len - src->pos)
#define AVAIL(X) GET_AVAIL >= X
#define CONSUME(X)                                   \
    ((const void *)((const unsigned char *)src->data + \
                    ((src->pos += X) - X)))

static char get_byte (Parser *src){
    if (!AVAIL(1))
        return 0;
    const unsigned char *byte = CONSUME(1);

    return *byte;
}

static bool get_bool (Parser *src){
    if (!AVAIL(1))
        return false;
    const unsigned char *byte = CONSUME(1);

    return *byte != 0;
}

static uint32_t get_uint32 (Parser *src){
    if (!AVAIL(4))
        return 0;

    const unsigned char *bytes = CONSUME(4);

    return GET_32BIT_MSB_FIRST(bytes);
}

static ptrlen get_string (Parser *src){
    size_t len = get_uint32(src);

    if (!AVAIL(len))
        return make_ptrlen("", 0);

    return make_ptrlen(CONSUME(len), len);
}

#endif /* _PARSER_H */
// vim: textwidth=100
