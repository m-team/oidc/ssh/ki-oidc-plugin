#ifndef _MARSHAL_H
#define _MARSHAL_H

#include "parser.h"

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    char *data;
    size_t len;
} strbuf;

strbuf *make_strbuf();
char *strbuf_to_str(strbuf *buf);
void free_strbuf(strbuf *buf);

void put_byte(strbuf *buf, char val);
void put_bool(strbuf *buf, bool val);
void put_uint32(strbuf *buf, uint32_t val);
void put_string(strbuf *buf, const char *val, size_t len);
// Only use for null terminated strings
void put_stringi(strbuf *buf, const char *val);
// Put string without length field in front
void put_stringnl(strbuf *buf, const char *val, size_t len);
void put_ptrlen(strbuf *buf, const ptrlen *ptr);

// Taken from putty
static inline void PUT_32BIT_MSB_FIRST (void *vp, uint32_t value){
    uint8_t *p = (uint8_t *)vp;

    p[3] = (uint8_t)(value);
    p[2] = (uint8_t)(value >> 8);
    p[1] = (uint8_t)(value >> 16);
    p[0] = (uint8_t)(value >> 24);
}

#endif /* _MARSHAL_H */
