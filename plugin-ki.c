#include "plugin-ki.h"
#include "lib/logc/log.h"

#include <stdlib.h>

int parse_plugin_init (Parser *src, plugin_ki_message *msg){
    log_debug("parse_plugin_init");
    plugin_ki_init *init = malloc(sizeof(plugin_ki_init));

    init->type     = PLUGIN_INIT;
    init->version  = get_uint32(src);
    init->hostname = get_string(src);
    init->port     = get_uint32(src);
    init->username = get_string(src);

    msg->init = init;
    /**log_debug("init: version:  %d", init->version);*/
    /**log_debug("init: hostname: >%s<", ptrlen_to_str(&init->hostname));*/
    /**log_debug("init: port:     %d", init->port);*/
    /**log_debug("init: username: >%s<", ptrlen_to_str(&init->username));*/

    return PLUGIN_INIT;
}

int parse_plugin_protocol (Parser *src, plugin_ki_message *msg){
    log_debug("parse_plugin_protocol");
    plugin_ki_protocol *protocol = malloc(sizeof(plugin_ki_protocol));

    protocol->type = PLUGIN_PROTOCOL;
    protocol->id   = get_string(src);

    msg->protocol = protocol;
    log_debug("protol-id: %s", ptrlen_to_str(&protocol->id));
    return PLUGIN_PROTOCOL;
}

void parse_user_response (Parser *src, plugin_ki_user_response *resp){
    log_debug("parse_user_response");
    resp->num_responses = get_uint32(src);
    resp->responses     = malloc(sizeof(ptrlen) * resp->num_responses);

    for (uint32_t i = 0; i < resp->num_responses; i++)
        resp->responses[i] = get_string(src);
    log_debug("user_response: num_responses %s", resp->num_responses);
    for (uint32_t i = 0; i < resp->num_responses; i++)
        log_debug("user_response: response[%d]: %s", i, resp->responses[i]);
}

int parse_ki_server_request (Parser *src, plugin_ki_message *msg){
    log_debug("parse_ki_server_request");
    plugin_ki_server_request *req = malloc(sizeof(plugin_ki_server_request));

    req->type         = PLUGIN_KI_SERVER_REQUEST;
    req->prompt_name  = get_string(src);
    req->instructions = get_string(src);
    req->language_tag = get_string(src);
    req->num_prompts  = get_uint32(src);
    req->prompts      = malloc(sizeof(plugin_prompt) * req->num_prompts);
    for (uint32_t i = 0; i < req->num_prompts; i++) {
        req->prompts[i].prompt       = get_string(src);
        req->prompts[i].safe_to_show = get_bool(src);
    }
    /**log_debug("request: prompt_name:  >%s<", ptrlen_to_str(&req->prompt_name));*/
    /**log_debug("request: instructions: >%s<", ptrlen_to_str(&req->instructions));*/
    /**log_debug("request: language_tag: >%s<", ptrlen_to_str(&req->language_tag));*/
    /**log_debug("request: num_prompts:  >%d<", req->num_prompts );*/
    /**for (uint32_t i = 0; i < req->num_prompts; i++) {*/
    /**  log_debug("request: prompt[%d]: >%s<", i, ptrlen_to_str(&req->prompts[i].prompt));*/
    /**  log_debug("request: safe[%d]:   >%d<", i, req->prompts[i].safe_to_show);*/
    /**}*/

    msg->server_request = req;

    return PLUGIN_KI_SERVER_REQUEST;
}

/*
 * Parses the keyboard-interactive message into the union type msg
 *
 * The content of msg must be freed by the caller
 * Use free_ki_message
 */
int parse_ki_message (const char *buf, plugin_ki_message *msg){
    uint32_t msg_len = GET_32BIT_MSB_FIRST(buf);

    // Length field of message does not include itself
    if (msg_len <= 0)
        return PLUGIN_INVALID_MESSAGE;
    buf += 4;
    Parser src;

    src.data = buf;
    src.len  = msg_len;
    src.pos  = 0;

    char type = get_byte(&src);

    log_debug("parse_ki_message (type: %d)", type);
    switch (type) {
    case PLUGIN_INIT:
        return parse_plugin_init(&src, msg);
    /**case PLUGIN_INIT_USER_RESPONSE:*/
    /**  msg->init_user_response = malloc(sizeof(plugin_ki_init_user_response));*/
    /**  msg->init_user_response->type = type;*/
    /**  parse_user_response(&src, msg->init_user_response);*/
    /**  return type;*/
    case PLUGIN_PROTOCOL:
        return parse_plugin_protocol(&src, msg);
    case PLUGIN_KI_SERVER_REQUEST:
        return parse_ki_server_request(&src, msg);
    case PLUGIN_KI_USER_RESPONSE:
        msg->user_response       = malloc(sizeof(plugin_ki_user_response));
        msg->user_response->type = type;
        parse_user_response(&src, msg->user_response);
        return type;
    case PLUGIN_AUTH_SUCCESS:
    case PLUGIN_AUTH_FAILURE:
        msg->empty       = malloc(sizeof(struct plugin_ki_empty_message));
        msg->empty->type = type;
        return type;
    default:
        return PLUGIN_INVALID_MESSAGE;
    }
}

void free_ki_message (plugin_ki_message *msg){
    log_debug("free_ki_message (type: %d)", *msg->type);
    switch (*msg->type) {
    case PLUGIN_INIT:
        free(msg->init);
        break;
    case PLUGIN_PROTOCOL:
        free(msg->protocol);
        break;
    case PLUGIN_KI_SERVER_REQUEST:
        free(msg->server_request->prompts);
        free(msg->server_request);
        break;
    case PLUGIN_KI_USER_RESPONSE:
        free(msg->user_response->responses);
        free(msg->user_response);
        break;
    default:
        free(msg->empty);
    }
    free(msg);
}
// vim: textwidth=100
