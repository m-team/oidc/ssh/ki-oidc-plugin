#ifndef _PLUGIN_KI_H
#define _PLUGIN_KI_H

#include "stdbool.h"
#include "stdint.h"
#include "parser.h"

#define PLUGIN_PROTOCOL_VERSION 2
#define PLUGIN_PUTTY_VERSION_KNOWN_TO_WORK "0.78"

#define PLUGIN_KI_AUTH_METHOD_ID "keyboard-interactive"

#define PLUGIN_INVALID_MESSAGE -1

#define PLUGIN_INIT 1
#define PLUGIN_INIT_RESPONSE 2
//#define PLUGIN_INIT_USER_REQUEST 3
//#define PLUGIN_INIT_USER_RESPONSE 4
#define PLUGIN_PROTOCOL 3
#define PLUGIN_PROTOCOL_ACCEPT 4
#define PLUGIN_PROTOCOL_REJECT 5
#define PLUGIN_AUTH_SUCCESS 6
#define PLUGIN_AUTH_FAILURE 7
#define PLUGIN_INIT_FAILURE 8

#define PLUGIN_KI_SERVER_REQUEST 20
#define PLUGIN_KI_SERVER_RESPONSE 21
#define PLUGIN_KI_USER_REQUEST 22
#define PLUGIN_KI_USER_RESPONSE 23

struct plugin_ki_empty_message {
    char type;
};

struct plugin_prompt {
    ptrlen prompt;
    bool safe_to_show;
};

typedef struct plugin_prompt plugin_prompt;

struct plugin_ki_init {
    char type;
    uint32_t version;
    ptrlen hostname;
    uint32_t port;
    ptrlen username;
};

struct plugin_ki_init_response {
    char type;
    uint32_t version;
    ptrlen username;
};

struct plugin_ki_protocol {
    char type;
    ptrlen id;
};

struct plugin_ki_server_request {
    char type;
    ptrlen prompt_name;
    ptrlen instructions;
    ptrlen language_tag;
    uint32_t num_prompts;
    plugin_prompt *prompts;
};

struct plugin_ki_init_user_request {
    char type;
    ptrlen prompt_name;
    ptrlen instructions;
    uint32_t num_prompts;
    plugin_prompt *prompts;
};

struct plugin_ki_server_response {
    char type;
    uint32_t num_responses;
    ptrlen *responses;
};

typedef struct plugin_ki_init plugin_ki_init;
typedef struct plugin_ki_init_response plugin_ki_init_response;
typedef struct plugin_ki_protocol plugin_ki_protocol;
typedef struct plugin_ki_init_user_request plugin_ki_init_user_request;
typedef struct plugin_ki_server_response plugin_ki_init_user_response;
typedef struct plugin_ki_empty_message plugin_ki_protocol_reject;
typedef struct plugin_ki_empty_message plugin_ki_protocol_accept;
typedef struct plugin_ki_server_request plugin_ki_server_request;
typedef struct plugin_ki_server_response plugin_ki_server_response;
typedef struct plugin_ki_server_request plugin_ki_user_request;
typedef struct plugin_ki_server_response plugin_ki_user_response;
typedef struct plugin_ki_empty_message plugin_ki_auth_success;
typedef struct plugin_ki_empty_message plugin_ki_auth_failure;

union plugin_ki_message {
    /* Convenience member to get the currently stored type */
    char *type;
    plugin_ki_init *init;
    plugin_ki_init_response *init_response;
    plugin_ki_init_user_request *init_user_request;
    plugin_ki_init_user_response *init_user_response;
    plugin_ki_protocol *protocol;
    plugin_ki_protocol_reject *protocol_reject;
    plugin_ki_protocol_accept *protocol_accept;
    plugin_ki_server_request *server_request;
    plugin_ki_server_response *server_response;
    plugin_ki_user_request *user_request;
    plugin_ki_user_response *user_response;
    plugin_ki_auth_success *auth_success;
    plugin_ki_auth_failure *auth_failure;
    /* Only used for memory deallocation purposes */
    struct plugin_ki_empty_message *empty;
};

/* Union which holds a pointer to the actual message stored in the corresponding member */
typedef union plugin_ki_message plugin_ki_message;

int parse_ki_message(const char *buf, plugin_ki_message *msg);
void free_ki_message(plugin_ki_message *msg);

#endif /* _PLUGIN_KI_H */
// vim: textwidth=100
