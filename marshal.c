#include "marshal.h"

#include <assert.h>

void *append (strbuf *buf, size_t len){
    char *ret;

    buf->data           = realloc(buf->data, buf->len + len);
    ret                 = buf->data + buf->len;
    buf->len           += len;
    buf->data[buf->len] = '\0';
    return ret;
}

void put_byte (strbuf *buf, char val){
    memcpy(append(buf, 1), &val, 1);
}

void put_bool (strbuf *buf, bool val){
    unsigned char cval = val ? 1 : 0;

    memcpy(append(buf, 1), &cval, 1);
}

void put_uint32 (strbuf *buf, uint32_t val){
    unsigned char cval[4];

    PUT_32BIT_MSB_FIRST(cval, val);
    memcpy(append(buf, 4), &cval, 4);
}

void put_string (strbuf *buf, const char *val, size_t len){
    assert((len >> 31) < 2);
    put_uint32(buf, len);
    memcpy(append(buf, len), val, len);
}

void put_stringi (strbuf *buf, const char *val){
    put_string(buf, val, strlen(val));
}

void put_stringnl (strbuf *buf, const char *val, size_t len){
    memcpy(append(buf, len), val, len);
}

void put_ptrlen (strbuf *buf, const ptrlen *ptr){
    char *str = ptrlen_to_str(ptr);

    put_string(buf, str, ptr->len);
}

strbuf *make_strbuf (){
    strbuf *buf = malloc(sizeof(strbuf));

    buf->data    = malloc(1);
    buf->data[0] = '\0';
    buf->len     = 0;
    return buf;
}

char *strbuf_to_str (strbuf *buf){
    char *str = malloc(buf->len + 1);

    memcpy(str, buf->data, buf->len);
    str[buf->len] = '\0';
    return str;
}

void free_strbuf (strbuf *buf){
    free(buf);
}
// vim: textwidth=100
